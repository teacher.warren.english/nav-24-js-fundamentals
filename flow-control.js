// flow control (conditional statements & loops)

// two main conditional statements

const score = 75

if (score >= 75) {
    console.log("DISTINCTION!");
} else if (score >= 50) {
    console.log("you passed");
} else {
    console.log("you failed");
}

const fruit = "APPLE"

switch (fruit) {
    case "apple": console.log("Fruit color is GREEN")
        break;
    case "banana": console.log("Fruit color is YELLOW");
        break;
    case "orange": console.log("Fruit color is ORANGE");
        break;
    default: console.log("Fruit color UNKNOWN");
}

// return, break, continue - control how we act within a code block / loop / conditional structure

// LOOPS!
// This is how we repeat tasks

// There are 5 types of loops in JavaScript

let count = 0

// while (pre-test) -> MIGHT never run at all
while(count < 5) {
    console.log("WHILE", count++);
}

// do...while (post-test) -> will ALWAYS run at least once
do {
    console.log("DO...WHILE", count++);
} while (count < 10)

// for (general purpose)
for (let i = 0; i < 5; i++)
    console.log("FOR", i);

// for...of (for looping through ARRAYS)
const myArray = ["snake", "mouse", "meerkat", "baboon"]
for (const currentItem of myArray) {
    console.log("FOR OF", currentItem)
}

const myObject = {
    studentFName: "John",
    studentLName: "Snow",
    location: "Winterfell",
}

// for...in (for looping through OBJECT properties)
for (const key in myObject) {
    console.log("FOR IN", myObject[key], key)
}

// you COULD do this:
// console.log(myObject["location"]);

// using a forEach() loop (also used for Arrays, like for...of)
myArray.forEach((item) => {
    console.log("FOREACH", item);
})


// preventing infinite loops
let preventInfinite = 0, username
// do {
//     username = prompt(`Enter username [${preventInfinite+1}]:`, "warren-west")
//     preventInfinite++
// } while ((!!username && username.toLowerCase() !== "quit") && preventInfinite < 5)