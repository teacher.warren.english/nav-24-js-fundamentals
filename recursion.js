// Sum up all the numbers from 1 to num.
// i.e. From num down to 1.
function sumUpNumbers(num) {
    // With recursion; so no loops!
    if (num > 1)
        return num + sumUpNumbers(num-1)
    else
        return 1


    // return num > 0 ? sumUpNumbers(num-1) : 1 // ternary operator (? :)
}

console.log(sumUpNumbers(5));
// 5 + 4 + 3 + 2 + 1 = 15

let count = 10
while (count > 0) {
    // do stuff
    count--
}