// non-primitives
// object

const animal = {
    // properties
    petName: "Gary",
    species: "Snail",
    city: "Bikini Bottom",
    owner: "Spongebob",
    posX: 0,
    posY: 0,
    // methods
    move(x, y) {
        this.posX += x
        this.posY += y
    },
    speak() {
        console.log("Meow");
    }
}


// array
const animals = [
    "elephant", "hippo", "giraffe"
]

// primitives
let number = 5
let fname = "Warren"
let isMarried = true

// accessing object properties
console.log(animal);
console.log(animal.city)

// invoking object methods
animal.speak()
animal.move(25, 26)

console.log(animal.posX, animal.posY);
console.log(animal.city);

// dynamically adding a property to the object
animal.color = "red"

console.log(animal.color);

animal.color = "green"

console.log(animal.color);
