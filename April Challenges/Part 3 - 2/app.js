const defaultSelectOption = document.getElementById("default-option")
defaultSelectOption.setAttribute("disabled", "true")

console.log(defaultSelectOption)

const API_URL = "https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks"
let drinks = []


async function getDrinksDataAsync() {
    const resp = await fetch(API_URL)

    console.log(resp);
    
    const json = await resp.json()
    
    console.log(json);
    
    drinks = json

    console.log(drinks);

    populateDrinksSelect(drinks)
}
getDrinksDataAsync()

function populateDrinksSelect(drinks) {
    // does stuff
    console.log(drinks)
    console.log("populateDrinks() invoked")
}