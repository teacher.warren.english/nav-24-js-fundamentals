const displayFirstNameEl = document.getElementById("display-fname")
const displayLastNameEl = document.getElementById("display-lname")
const displayAgeEl = document.getElementById("display-age")

const API_URL = "https://example.com/api/people"
let result = {}


function getDataFromAPI() {
    fetch(API_URL)
        .then(resp => resp.json())
        .then(json => {
            result = json
            renderDataOnUI()
        })
        .catch(error => console.error(error))
}

function renderDataOnUI() {
    displayFirstNameEl.innerText = result.fname
    displayLastNameEl.innerText = result.lname
    displayAgeEl.innerText = result.age
}


getDataFromAPI()