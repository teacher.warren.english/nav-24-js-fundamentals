// const inputNumberEl = document.getElementById("input-number")

// inputNumberEl.addEventListener("change", event => handleChange(event))


// function handleChange(event) {
// //   console.log("Input number changed")
//   console.log(event.target.value)
//   console.log(isNaN(event.target.value));
//   console.log(!!(event.target.value));

//   console.log(Number(event.target.value) - 5)
// }



// ================================================

const inputNumberEl = document.getElementById("input-number")
const btnAddNumberEl = document.getElementById("btn-add-to-list")
const outputHighestEl = document.getElementById("output-highest")


btnAddNumberEl.addEventListener("click", handleAddNumber)


let numbers = []
let highest


/**Add new number to list and display the highest number. */
function handleAddNumber() {
    if (!inputNumberEl.value) return

    if (!highest) highest = -Infinity

    numbers.push(inputNumberEl.value)
    for (const n of numbers)
        if (n > highest)
            highest = n
    outputHighestEl.innerText = `Highest: ${highest}`
}


