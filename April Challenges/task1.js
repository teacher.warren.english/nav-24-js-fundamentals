function isEven(num) {
    if (num / 2 == 0)
        return true
    else
        return false
}

// transform this code to use JS ternary operator ( ? : )
function isOdd(num) {
    // fundamental code
    // if (num % 2 == 1)
    //     return true
    // else 
    //     return false

    // second shortest solution
    // return num % 2 == 1 ? true : false

    // shortest solution
    return num % 2 == 1
}

const isOdd = num => num % 2 == 1