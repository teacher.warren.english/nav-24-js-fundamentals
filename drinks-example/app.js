// DOM ELEMENTS
const drinksSelectEl = document.getElementById("drinks-select")
const drinkPriceEl = document.getElementById("drink-price")
const defaultSelectOption = document.getElementById("default-option")

// GLOBAL VARS
let drinks = []
const API_URL = "https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks"

// EVENT LISTENERS
drinksSelectEl.addEventListener("change", (event) => handleSelectDrink(event))

// EVENT HANDLERS


function handleSelectDrink(event) {
    console.log("handleSelectDrink(event) - drinks", drinks[0].id);
    console.log("handleSelectDrink(event) - event.target.value", event.target.value);
    
    // find the selected drink from memory using the event target value
    let selectedDrink = drinks.find((d) => d.id === Number(event.target.value))
    
    console.log(selectedDrink);
    
    // render the selected drink price to the screen
    drinkPriceEl.innerText = `Price: $${selectedDrink.price}` // if doing this was more complex, it would be best to separate it into its own function
    
    // disable the default option in the select box
    defaultSelectOption.setAttribute("disabled", "true")
}

// FUNCTIONS

async function getDrinksDataAsync() {
    const resp = await fetch(API_URL)
    const json = await resp.json()
    
    console.log("getDrinksDataAsync() - json", json);
    drinks = json
    
    console.log("getDrinksDataAsync() - drinks", drinks);
    populateDrinksSelect(drinks)
}

function populateDrinksSelect(drinks) {
    console.log("populateDrinksSelect(drinks) - drinks", drinks);
    for (const item of drinks) {
        let newOption = document.createElement("option")
        newOption.value = item.id
        newOption.innerText = item.description

        drinksSelectEl.appendChild(newOption)
    }
}

// RUNTIME

getDrinksDataAsync()