const myFunc = () => { return 42 }
const array = [ 
    12,
    "12",
    null,
    undefined,
    {name: "warren"},
    [ 0, 1, 2 ],
    myFunc,
    myFunc()
]

for (const item of array) {
    console.log(typeof item);
}

// =============
// OTHER STUFF

if (isNaN(number)) {}

if (typeof number === "number") {}

console.log(typeof x);
console.log(typeof 0);
if (x == 0) {}