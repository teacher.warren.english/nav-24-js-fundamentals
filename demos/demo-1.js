// assume 0 is positive

let numbers = [44, 57, 31, 42, -22, 0, 10, -3, -7, 6, 42]

// using the numbers array, build new arrays:
// 1. an array of positive numbers
// 2. negative
// 3. odd
// 4. even
function getNegNums() {
    // init output array
    let output = []

    // loop through all the numbers in the array
    for (const n of numbers) {
        // check if n is positive
        // if it is, push it onto the output array
        if (n < 0)
            output.push(n)
    }

    return output
}
function getOddNums() {
    // init output array
    let output = []

    // loop through all the numbers in the array
    for (const n of numbers) {
        // check if n is positive
        // if it is, push it onto the output array
        if (n % 2 != 0)
            output.push(n)
    }

    return output
}

function checkNumber(logicalCheck) {
    // init output array
    let output = []

    // loop through all the numbers in the array
    for (const n of numbers) {
        // check if n is even
        // if it is, push it onto the output array
        if (logicalCheck(n))
            output.push(n)
    }

    return output
}

// number check arrow functions
const checkIsEven = num => num % 2 == 0
const checkIsOdd = num => !checkIsEven(num)
const checkIsPositive = num => num >= 0
const checkIsNegative = num => !checkIsPositive(num)
const checkIsFortyTwo = num => num === 42

const checkFunctions = [
    checkIsEven,
    checkIsOdd,
    checkIsPositive,
    checkIsNegative,
    checkIsFortyTwo,
]

for (const x of checkFunctions) {
    console.log(checkNumber(x))
}

// console.log(checkNumber(checkIsEven))
// console.log(checkNumber(checkIsOdd))
// console.log(checkNumber(checkIsPositive))
// console.log(checkNumber(checkIsNegative))
// console.log(checkNumber(checkIsFortyTwo))


function testNumber() {
    let userInput, outputMessage = ""
    userInput = prompt("Enter a number:", 3)

    if (Number(userInput) % 2 == 0) {
        // number is even
        // check if positive or negative
        if (Number(userInput) >= 0) {
            // number is positive and even
            outputMessage = `${userInput} is positive and even`
        } else {
            // number is negative and even
            outputMessage = `${userInput} is negative and even`
        }
        
        // return the correct message
        
    } else {
        // number is odd
        // check if positive or negative
        if (Number(userInput) >= 0) {
            // number is positive and odd
            outputMessage = `${userInput} is positive and odd`
        } else {
            // number is negative and odd
            outputMessage = `${userInput} is negative and odd`
        }
        
        // return the correct message
    }
    alert(outputMessage)
}

// console.log(getPosNums())

// testNumber()