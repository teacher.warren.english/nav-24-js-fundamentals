// DOM ELEMENTS
const btnAddNameElement = document.getElementById("btn-add-name")
const inputNameElement = document.getElementById("input-name")
const listNamesElement = document.getElementById("names-list")

// GLOBAL VARIABLE
let classList = [
    "Warren",
    "Sarah",
    "James",
]

// EVENT LISTENERS
btnAddNameElement.addEventListener("click", handleAddNameClick)

// EVENT HANDLERS
function handleAddNameClick() {
    // basic validation
    if (!inputNameElement.value) return

    classList.push(inputNameElement.value)

    // render out the names
    renderNamesList()
}

function handleChangeColor(event) {
    console.log(event.target);

    event.target.setAttribute("style", "color: red;")
    // event.target.setAttribute("style", "display: none;")
}

// FUNCTIONS
function renderNamesList() {
    console.log("renderNamesList()");
    console.log(classList);

    listNamesElement.innerHTML = ''

    // loop through class list names
    for (const name of classList) {
        const newListItem = document.createElement("li")
        newListItem.innerText = name

        // add event listener to the new item
        newListItem.addEventListener("click", (event) => handleChangeColor(event))

        // for each name, add it to the list
        listNamesElement.appendChild(newListItem)
    }
}

// RUNTIME
renderNamesList()