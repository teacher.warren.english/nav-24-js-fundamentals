// named exports:
export function addThreeNumbers(num1, num2, num3) {
    return num1 + num2 + num3
}

export const squareOneNumber = (num) => {
    return num * num
}

export const myVariable = true
export const API_URL = "https://whatever/api"

// default export:
function myFunction() {
    return true
}

export default myFunction