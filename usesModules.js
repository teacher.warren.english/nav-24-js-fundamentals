import { addThreeNumbers as yyy, squareOneNumber } from "./modulesWeUse.js";
import theFunction from './modulesWeUse.js'
import { printMessage } from "./anotherModuleWeUse.js"

console.log("This is a JavaScript files that uses modules.");

let result = yyy(5, 10, 15)

console.log(result);

let squaredResult = squareOneNumber(10)

console.log(squaredResult);

printMessage()

console.log(theFunction())