
// declared function
// returns a number value
// This is hoisted
function addTwoNumbers(num1, num2) {
    let result = num1 + num2
    
    return result
}

// function expression
// kinda like a function declared as a variable?
const addTwoValues = function(num1, num2) {
    return num1 + num2
}

console.log(addTwoNumbers(2, 3));
console.log(addTwoValues(4, 5));

// void functions
function printFavNumberToConsole(favNum) {
    console.log(favNum);
}

let voidReturnValue = printFavNumberToConsole(5)

console.log(voidReturnValue);

// Arrow functions () => {}

const addTwoNumbersArrow = (num1, num2) => {
    let result = num1 + num2
    return result
}

const addTwoValuesArrow = (num1, num2) => num1 + num2

// invoke arrow function
console.log(addTwoValuesArrow(9, 9));

const printFavNumberToConsoleArrow = favNum => console.log(favNum)
// invoking the arrow function
printFavNumberToConsoleArrow(15)

// const funcB = function (a, b, c) {
//     return a + b + c
// }

// function myFuncThatReceivesFuncB(funcB) {
//     console.log(funcB(1, 2, 3));
// }

// myFuncThatReceivesFuncB(funcB)

const fizzbuzzArrow = num => {

    // check if num is a multiple of 3 AND 5
    if (num % 3 === 0 && num % 5 === 0)
        return "FIZZBUZZ"

    if (num % 3 === 0)
        return "FIZZ"

    if (num % 5 === 0)
        return "BUZZ"

    return num
}

console.log(fizzbuzzArrow(5) )   // Expected output: B
console.log(fizzbuzzArrow(15))   // Expected output: FB
console.log(fizzbuzzArrow(8) )   // Expected output: 8
console.log(fizzbuzzArrow(9) )   // Expected output: F
console.log(fizzbuzzArrow(-2) )  // Expected output: -2