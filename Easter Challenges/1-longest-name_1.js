let namesList = []

// prompt the user for a list of names one by one
function getNames() {
    let current, output = [], count = 0;
    do {
        current = prompt(`Enter new name [${count+1}]:`, "Jack")
        output.push(current)
        // console.log(`count: ${count}`);
        // console.log(`output[${count}]: ${output[count]}`);    
        count++
    } while (current.toLowerCase() != "quit")
    
    output.pop()
    return output // return the array without "quit" on the end
}

// return the longest name in the array
function getLongestName(names) {
    let longest = names[0]
    for (const name of names) {
        if (name.length > longest.length)
            longest = name
    }

    return longest
}

namesList = getNames() // retrieve names
console.log(namesList) // log names
console.log(`Longest name: ${getLongestName(namesList)}`) // output longest name