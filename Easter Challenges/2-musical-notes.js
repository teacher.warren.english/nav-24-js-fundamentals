const NOTES = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
const POS_A = NOTES.findIndex((n) => n == "A")

// works
function createMusicalArray1(numOctaves) {
    const output = []
    for (let octave = 1; octave <= numOctaves; octave++)
        for (let note = 0; note < NOTES.length; note++)
            output.push(`${NOTES[note]}${note < POS_A ? octave : octave + 1}`) // ternary operator (? :)

    return output
}

function createMusicalArray2(numNotes) {
    const musicalArray = []
    let currentOctave = 1
    let noteIndex = 0

    while (musicalArray.length < numNotes) {
        musicalArray.push(`${NOTES[noteIndex]}${noteIndex < POS_A ? currentOctave : currentOctave + 1}`)
        noteIndex++

        if (noteIndex >= NOTES.length) {
            noteIndex = 0
            currentOctave++
        }
    }

    return musicalArray
}

// works
function createMusicArrayWithStart(numOctaves, start) {
    // basic validation
    if (!numOctaves || !start)
        return []

    // init variables
    const [startNote, startOctave, output] = [start.slice(0, start.length - 1), start.slice(start.length - 1), []]

    // find start position
    let startPos = NOTES.findIndex((n) => n == startNote)
    let counter = 0

    console.log(`Start Position: ${startNote}${startOctave}`)

    // basic validation
    if (startPos == -1)
        return []

    // loop through the octaves
    for (let octave = Number(startOctave); octave < Number(startOctave) + Number(numOctaves); octave++) {
        // loop the notes in a single octave
        for (let note = 0; note < NOTES.length; note++) {
            // Circle back to the start of the notes array if we go past the final index
            // and also increment the counter when the line has finished executing
            let modVal = (startPos + counter++) % NOTES.length
            // console.log(`${modVal}`)
            // Conditional to decide if the octave has shifted up
            output.push(`${NOTES[modVal]}${modVal < POS_A && modVal >= startPos ? octave : octave + 1}`)
        }
    }

    return output
}

// Examples
// console.log(createMusicalArray1(3))
// console.log(createMusicalArray2(18))
console.log(NOTES);
console.log(createMusicArrayWithStart(2, "E2"))