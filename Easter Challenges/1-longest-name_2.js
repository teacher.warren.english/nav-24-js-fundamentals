let nameList = [
    "Gaute",
    "Warren",
    "Elisabeth",
    "Max",
    "Bjorn",
    "Jack",
]

const findLongestName = (nameList) => {
    let output = [nameList[0], 0] // array [longestName, index]
    for (let i = 1; i < nameList.length; i++)
        if (nameList[i].length >= output[0].length)
            output = [nameList[i], i]

    return output
}

const orderByNameLength = (nameList) => {
    let orderedList = [] // results
    let loopLength = nameList.length - 1 // create this variable, because as we remove elements from the nameList array, it's length gets shorter

    for (let i = 0; i < loopLength; i++)
        orderedList.push(nameList.splice(findLongestName(nameList)[1], 1)[0])

    orderedList.push(nameList[0])
    return orderedList
}

console.log(findLongestName(nameList)) // if you call this method second, use a slice() to pass by value
console.log(orderByNameLength(nameList))