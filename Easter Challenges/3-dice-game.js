// returns a random number between 1 and 6 (inclusive)
function rollDie() {
    return Math.trunc((Math.random() * 6) + 1)
}

const gameTurns = 5 // number of turns in a single game
const playerOptions = `1. Play new game\n2. View leader board\n\n"Quit" to exit`

let leaderBoard = [] // array of objects [{username, score}, ...]

function gameLoop(username) {
    // init game
    let score = 0
    console.clear()

    // loop 5 turns
    for (let i = 0; i < gameTurns; i++) {
        console.log(`Round ${i + 1}, lets go!`)
        let firstRoll, secondRoll
        // do
        do {
            // first roll
            firstRoll = rollDie()
            // second roll
            secondRoll = rollDie()
            // add dice tally to score
            console.log(`You rolled ${firstRoll} and ${secondRoll} to score ${firstRoll + secondRoll} points!`)
            if (firstRoll === secondRoll)
                console.info("AWESOME! You rolled a double, take another turn...")
            score += firstRoll + secondRoll
            console.log(`Current score: ${score} points!`)
        } while (firstRoll === secondRoll)
        // while first and second rolls are equal
    }

    // display score
    console.log(`Congratulations ${username}, you scored ${score} points!`)

    // add record to leader board
    // a record is {username, score}
    addNewScore(username, score)
}

function mainMenu() {
    // enter options loop
    let userInput

    do {
        userInput = prompt(playerOptions, "1")
        if (!userInput) userInput = "quit"

        switch (userInput.toLowerCase()) {
            case "1": gameLoop(prompt("Enter username:", "WTW"))
                break
            case "2": viewLeaderBoard()
                break
            case "quit": alert("Ending the game...")
                break
            default: alert(`"${userInput}" not recognised, please try again...`)
                break
        }
    } while (userInput.toLowerCase() != "quit")
}

// Push a new record object into the leader board array
function addNewScore(username, score) {
    // let newObject = {}
    // newObject.username = username
    // newObject.score = score
    // leaderBoard.push(newObject)
    leaderBoard.push({ username, score })
}

// Print out the leader board to the console
function viewLeaderBoard() {
    console.clear()

    // no scores on leader board
    if (leaderBoard.length < 1) {
        console.log("No records to display...")
        console.log("Play a game")
        return
    }

    // leader board console header
    console.log("LEADER BOARD:")

    // only one score
    if (leaderBoard.length == 1) {
        displaySingleScore(leaderBoard[0], true)
        return
    }

    // multiple scores on leader board
    let topScore = findTopScore()

    for (let i = 0; i < leaderBoard.length; i++)
        displaySingleScore(leaderBoard[i], i === topScore[1])
}

// finds the top score and returns an array containing:
// 0: the top score
// 1: the index of the top score
function findTopScore() {
    let topScore = [leaderBoard[0].score, 0]

    for (let i = 1; i < leaderBoard.length; i++)
        if (leaderBoard[i].score > topScore[0])
            topScore = [leaderBoard[i].score, i]

    return topScore
}

// Display a single score record
// Formatting depends on whether it's the top score
function displaySingleScore(record, isTopScore) {
    if (isTopScore)
        console.log(`${record.username}\t${record.score} * TOP SCORE`)
    else
        console.log(`${record.username}\t${record.score}`)
}

mainMenu()