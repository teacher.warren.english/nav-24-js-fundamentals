const myFirstArray = [
    0,
    55, 
    14, 
    68, 
    21, // trailing comma
]

// mutable vs immutable
// immutable = we cannot REASSIGN the value

// myFirstArray.sort()

// push: add a new element to the end of the array
let newLength = myFirstArray.push(100) // =
// console.log(newLength);

// pop: remove the last element from the array
let popReturn = myFirstArray.pop()
// console.log(popReturn);

// getting the array length property
// console.log(myFirstArray.length);

console.log(myFirstArray);
// console.log(myFirstArray.length);
// console.log(myFirstArray[myFirstArray.length-1])

// Array.Prototype.[helper functions]

// copy a subset of the array
let subset = myFirstArray.slice(0, 2) // incl, excl
// console.log(subset);
// console.log(myFirstArray);

// splice
let spliceReturn = myFirstArray.splice(2, 2, subset[0], subset[1], 4, 34, 36)

console.log(myFirstArray);
console.log(spliceReturn);

console.log("hello", 1, 2, 3, 4, "another argument", false, true, true)







// sidetrack

let myString = "hello world"
let substring = myString.substring(2, 4) // incl, excl

// console.log(substring);