console.log("Hello from an external file!")

// --------------------------------


let message = ""

message = "I am a mutable variable - I can change!"

// JavaScript is a dynamically typed language
// AKA weakly typed language
// values do not have to stick to a specific TYPE (string, number, boolean, undefined, null)

message = 3.142

message = false

message = 10

message = [ 13, false, 29.2, "hello", [1, 2, "this is weird"] ]

console.log(message)

// --------------------------------

const myNumber = 55

// myNumber = 32 // illegal!


// --------------------------------
// Code blocks

{
    var count = "VAR variable"
    let myInnerVariable = 10
    const myInnerConst = "Hey whatsup"

    while () {
        let count = 0


        count++
    }

    
    console.log(myNumber)
}

console.log(count);
console.log(myInnerConst);
console.log(myInnerVariable);

// NAMING CONVENTIONS
// camelCasing -> variables, functions, classes
// PascalCasing -> Functional Constructors
// lowercase_snake_casing
// UPPERCASE_SNAKE_CASING -> Constants (True constants)
// lowercase-kebab-casing
// UPPERCASE-KEBAB-CASING