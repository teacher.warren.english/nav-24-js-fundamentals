// DOM ELEMENTS

const inputMultipleEl = document.getElementById("input-multiple")
const btnCountDownEl = document.getElementById("count-down")
const btnCountUpEl = document.getElementById("count-up")
const countOutputEl = document.getElementById("count-output")
const tallyHistoryListEl = document.getElementById("tally-history")

// GLOBAL VARIABLES

let count = 0
let tallyHistory = [
    { num: "0", isIncrement: true },
]

// standardized text for the "add" and "subtract" buttons
const btnCountDownText = "-"
const btnCountUpText = "+"

// EVENT LISTENERS

// btnCountUpEl.addEventListener("click", handleCountUp)
// btnCountDownEl.addEventListener("click", handleCountDown)
btnCountUpEl.addEventListener("click", (event) => handleUpdateTally(event))
btnCountDownEl.addEventListener("click", (event) => handleUpdateTally(event))

// EVENT HANDLERS

/**Increment from the tally and update input history. */
function handleCountUp() {
    // basic input validation
    if (!inputMultipleEl.value) return

    let newHistoryRecord = {}
    newHistoryRecord.num = inputMultipleEl.value
    newHistoryRecord.isIncrement = true

    tallyHistory.push(newHistoryRecord)
    count += Number(newHistoryRecord.num)
    countOutputEl.innerText = `Tally: ${count}`

    newDisplayHistory()
}

/**Decrement from the tally and update input history. */
function handleCountDown() {
    // basic input validation
    if (!inputMultipleEl.value) return

    let newHistoryRecord = {}
    newHistoryRecord.num = inputMultipleEl.value
    newHistoryRecord.isIncrement = false

    tallyHistory.push(newHistoryRecord)
    count -= Number(newHistoryRecord.num)
    countOutputEl.innerText = `Tally: ${count}`

    newDisplayHistory()
}

/**A generic function to update the tally and input history. Receives the event object. */
function handleUpdateTally(event) {
    // input validation
    if (!inputMultipleEl.value) return
    
    // init new history record
    let newHistoryRecord = {}
    newHistoryRecord.num = inputMultipleEl.value
    
    // check to see if the "add" or "subtract" button was clicked
    if (event.target.innerText === btnCountDownText) {
        console.log(event.target.id) // it would be a better idea to base this conditional logic off the event.target.id instead of event.target.innerText
        // "subtract" was clicked
        newHistoryRecord.isIncrement = false
        count -= newHistoryRecord.num
    } else if (event.target.innerText === btnCountUpText) {
        // "add" was clicked
        newHistoryRecord.isIncrement = true
        count += Number(newHistoryRecord.num)
    } else {
        // unidentified option was clicked :O
    }
    
    tallyHistory.push(newHistoryRecord)
    countOutputEl.innerText = `Tally: ${count}`
    
    newDisplayHistory()
}

// FUNCTIONS

/**Re-render the input history. */
function displayHistory() {
    tallyHistoryListEl.innerHTML = "" // first clear the previous history, then loop through the array
    
    for (const item of tallyHistory) {
        tallyHistoryListEl.innerHTML += `<li>${item}</li>`
    }
}

/**Re-render the input history. The input history is color-coded according to incremental (green) or decremental (red) values. */
function newDisplayHistory() {
    tallyHistoryListEl.innerHTML = "" // first clear the previous history, then loop through the array

    for (const item of tallyHistory) {
        let newLiItem = document.createElement("li")

        if (item.isIncrement)
            newLiItem.setAttribute("style", "color: green;")
        else
            newLiItem.setAttribute("style", "color: red;")

        newLiItem.innerText = item.num
        tallyHistoryListEl.appendChild(newLiItem)
    }
}

/**Use a standardized way to assign button text to "add" and "subtract" buttons. */
function setAddSubtractButtonsText() {
    btnCountDownEl.innerText = btnCountDownText
    btnCountUpEl.innerText = btnCountUpText
}

// RUNTIME

setAddSubtractButtonsText()