/*
    arrayOfMultiples(7, 5) ➞ [7, 14, 21, 28, 35]
    arrayOfMultiples(12, 10) ➞ [12, 24, 36, 48, 60, 72, 84, 96, 108, 120]
    arrayOfMultiples(17, 6) ➞ [17, 34, 51, 68, 85, 102]
*/

function arrayOfMultiples(num, length) {
    // basic validation
    if (!num || !length || num < 1 || length < 1 || isNaN(length) || isNaN(num)) {
        console.error("Your input parameters should not be null, empty, or undefined. And they should be greater than 0.")
        return []
    }

    if (num === true || length === true) {
        console.error("Number and length cannot be Boolean values!")
        return []
    }

    // create an output variable (empty array)
    let output = []
    //output = Array(length).fill("temp")

    // run a loop from 0 or 1? to length
    for (let i = 1; i <= length; i++) {
        // inside the loop, we need to populate the array
        // with multiples of num
        output.push(i * num)
    }

    // log the output array before returning it (admin)
    console.log(output);

    // return the output array (hopefully filled with some multiples)
    return output
}

// invoke the arrayOfMultiples() function
arrayOfMultiples(7, 5)
arrayOfMultiples(12, 10)
arrayOfMultiples(17, 6)
// Test validation
// arrayOfMultiples(17, 0)
// arrayOfMultiples(0, 6)
// arrayOfMultiples(-5, 6)
// arrayOfMultiples("ajhsdfv", 6)
// arrayOfMultiples(10, false)
// arrayOfMultiples(10, true)