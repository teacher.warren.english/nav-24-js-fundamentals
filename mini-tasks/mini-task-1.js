// validation of inputs ?
// loop until valid input is received

let salary = prompt("Enter salary:", 1000)
let tax = prompt("Enter tax:", 25)

function processSalary() {
    // basic salary validation
    if (!salary || salary <= 0 || isNaN(salary)) {
        console.error("Invalid salary")
        return {}
    }
    // basic tax validation
    if (!tax || tax <= 0) {
        console.error("Invalid tax")
        return {}
    }

    // if the salary is 1000
    // and the tax is 25%
    // then tax prop = 250
    // and the net prop = 750
    const salaryOutput = {
        tax: salary * (tax / 100), // + "10" + 20 = 1020
        net: salary - (salary * (tax / 100)),
    }

    return salaryOutput
}

function processFiftyThirtyTwenty(netSalary) {
    // basic validation
    if (!netSalary) {
        console.error("Invalid net salary")
        return {}
    }

    const fiftyThirtyTwentyOutput = {
        fifty: netSalary * 0.5,
        thirty: netSalary * 0.3,
        twenty: netSalary * 0.2,
    }

    console.log(fiftyThirtyTwentyOutput);

    return fiftyThirtyTwentyOutput
}

console.log(`Salary: ${salary} \nTax: ${tax}`);

let splitSalary = processSalary()
let splitNetSalary = processFiftyThirtyTwenty(splitSalary.net)

console.log(splitSalary); // object - tax & net props
console.log(splitNetSalary); // object - fifty, thirty, twenty props