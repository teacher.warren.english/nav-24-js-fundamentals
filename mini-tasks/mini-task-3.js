// Retrieve drinks data from the drinks API
// Then prompt the user with a drinks menu
// The user should be allowed to select one or many drinks
// After selection, the user should be presented with the total amount owed for the drinks

const DRINKS_API_URL = "https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks"
let drinks = [] // list of drinks from API
let order = [] // list of drink ids selected by the user

// use ES6 Promise syntax to get JSON data from API
function getDataWithPromise() {
    fetch(DRINKS_API_URL)
        .then(resp => resp.json())
        .then(json => {
            drinks = json
            runSelectionLoop()
            displayReceipt()
        })
        .catch(error => console.error(error))
}

// display a prompt to the user with a drinks selection
// return the user's selection (typically a drink id or 'q')
function displayMenu() {
    let message = `Please select a drink:\n`

    for (let i = 0; i < drinks.length; i++) {
        message += `\n${drinks[i].id}. ${drinks[i].description}\t${formatCurrency(drinks[i].price)}`
    }

    message += `\n\n"q" to quit`

    return prompt(message)
}

// loop infinitely until the user enter's 'q'
function runSelectionLoop() {
    let selection

    do {
        // add the id of the selected drink to orders
        selection = displayMenu()
        // user chooses to exit
        if (selection.toLowerCase() == 'q') return
        // basic validation (selection is not 'q', and is not a valid id)
        if (drinks.findIndex(item => item.id == selection) === -1) continue // TODO: should display some kind of warning message

        // selection is not q, and is valid
        order.push(selection)
    } while (selection.toLowerCase() != 'q')
}

// display the receipt to the user
function displayReceipt() {
    let amount = 0, message = ''

    // loop through the list of ids
    for (const id of order) {
        let drink = drinks.find(item => item.id == id)
        
        amount += drink.price
        message += `\n${drink.description}\t${formatCurrency(drink.price)}`
    }

    message += `\n\nAmount owed: ${formatCurrency(amount)}`

    alert(message)
}

// consistently format the currency
function formatCurrency(num) {
    return `${Number(num)} Kr.` // TODO: make sure we're always using 2 decimal points
}

// getDataWithPromise() // main "runtime" function