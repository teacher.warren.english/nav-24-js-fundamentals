let [fname, lname, subjects] = ["Warren", "West", ["X","Y","Z"]]
const favColors = [
    "Blue",
    "Red",
    "Green",
]

// BEFORE
const student1 = {
    studId: 1,
    fname: fname,
    lname: lname,
    subjects: subjects,
    favColors: favColors,
}

// AFTER
const student2 = {
    studId: 1,
    fname, // <-- Object Literals
    lname, // <-- Object Literals
    subjects, // <-- Object Literals
    favColors, // <-- Object Literals
}


console.log(student1);
console.log(student2);