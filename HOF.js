// Higher Order Functions
// Functions that receive functions as arguments

// init array of numbers
let numbers = [ 3, 7, 12, 5, 10 ]

// .map()
// transforms all elements of an array
// returns a new array with the same number of elements as the original
let newArray = numbers.map(num => num * 2)

// console.log("numbers", numbers);
// console.log("newArray", newArray);

// .filter()
let evenNumbers = numbers.filter((num) => num % 2 == 0)

// ternary operator ( ? : )
let oddNumbers = numbers.filter(num => num % 2 == 1 ? true : false)

// console.log(evenNumbers);
// console.log(oddNumbers);

// .reduce()
// Returns a single value. The original array is reduced into a single value, e.g. summing up numbers
let sumOfNumbers = numbers.reduce((prev, current) => prev + current, 0)

// console.log(sumOfNumbers);

// .sort()
let sortedNumbers = numbers.sort((a, b) => b - a)
console.log(sortedNumbers);

let arrNames = [
    "Abcd",
    "Abc",
    "Abcde",
    "Ab",
]

let sortedNames = arrNames.sort((a, b) => {
    return b.length - a.length
})

console.log(sortedNames);