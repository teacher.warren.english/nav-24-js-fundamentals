let fname
let lname
let age
let breakFromLoop = true
let continueLoop = true

for (let i = 0; i < 10; i++) {
    if (i % 2 == 0) continue;
    
    console.log(i);
}

// console.log(fname)
// console.log(lname)
// console.log(age)

console.log("THIS IS THE START OF THE DO WHILE");

let count = 0
do {
    if (count++ % 2 == 0) continue; // if we don't do a count++ here, we get an infinite loop because the code below that changes the value of count will never be reached
    
    console.log(count);
    // count++
} while (count < 10)