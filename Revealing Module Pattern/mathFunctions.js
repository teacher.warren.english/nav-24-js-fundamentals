function doubleNumber(num) {
    return num * 2
}

function tripleNumber(num) {
    return secretAlgorithm(num) * 0.75
}

// "private"
function secretAlgorithm(num) {
    return num * 4
}

// emulating / exposing "public" things
const math = {
    doubleNumber, // object literals
    tripleNumber, // object literals
}

export default math