let location = ""
let diet = []
let age
let weight
let BMI

function setLocation(newLocation) {
    location = newLocation
}

// TODO: function arguments
function setDiet(doDisplay, ...newDiet) {
    // WARNING: PASS BY REF!
    // diet = [...newDiet]
    // diet = newDiet.slice()
    console.log("doDisplay:", doDisplay);
    if (doDisplay)
        for (const item of newDiet)
            console.log(item)
    else {
        // do something else without displaying the data
    }

}

function calculateAnimalBMI() {
    // basic validation
    if (isNaN(age) || isNaN(weight) || diet.length === 0)
        return

    BMI = (age * weight) / diet.length
}

// other setters

function displayDetails() {
    calculateAnimalBMI()
    console.log(`ANIMAL DETAILS:
---------------------
Location: ${location}
Diet: ${diet}
Age: ${age}
Weight: ${weight}
BMI: ${BMI}
---------------------`)
}

const animal = {
    setLocation,
    setDiet,
    // other public setters
    displayDetails,
}

export default animal