// let firstname = "James"
// let lastname = "Johnson"
// let age = 42

// let favColor;
// favColor = "Blue"

let [fname, lname, age, favColor] = ["James", "Johnson", 42, "Blue"]

console.log(fname)
console.log(lname)
console.log(age)
console.log(favColor)

// let my_api_URL = "https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks"
// let searchHistory = ["", "", ""]

// this is not the best use of destructuring syntax because these variables are totally unrelated
// and also, the API URL would usually be a const, that's stored in a separate .js file
let [my_api_URL, searchHistory] = ["https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks", ["", "", ""]]



// ------------------------------------------------------------
// Destructuring Objects

let student = {
    studentNumber: 1,
    subjects: [
        "Computer Programming 101",
        "Computer Assembly 101",
        "Game Development 101",
    ],
    isStudentLeader: false,
    assistantDroid: {
        modelNumber: "C3PO",
        color: "Gold",
        originPlanet: "Tatooine / Affa / unknown"
    }
}

// const studentNumber = student.studentNumber
const { studentNumber: studNo } = student

// let subjects = student.subjects
let { subjects: subs } = student


// let originPlanet = student.assistantDroid.originPlanet
let { originPlanet } = student.assistantDroid


console.log(studNo);
console.log(subs);
console.log(originPlanet);

// ------------------------------------------------------------
// Destructuring Arrays

let arrayOfNumbers = [22, 16, 55, 10, 81, 42, 76, 58]

let firstElement = arrayOfNumbers[0]
let secondElement = arrayOfNumbers[1]

// let oddNumbers = []
// oddNumbers.push(arrayOfNumbers[2], arrayOfNumbers[4])

let oddNumbers = [arrayOfNumbers[2], arrayOfNumbers[4]]

// Remember: if you are trying to declare more variables than elements in the array, the "left-over" variables will be undefined.
let [firstNumber, secondNumber, thirdNumber] = arrayOfNumbers
let fourthNumber;

console.log("DESTRUCTURING ARRAYS");
console.log(firstNumber, secondNumber, thirdNumber);

// before log
console.log("firstElement", firstElement);
console.log("secondElement", secondElement);

// Let's swap the values of firstElement and secondElement
[firstElement, secondElement] = [secondElement, firstElement]

// after log
console.log("firstElement", firstElement);
console.log("secondElement", secondElement);




// ------------------------------------------------------------
// Real world demo (Array Destructuring)

function specialFunc() {
    // init function output
    let result

    try {
        // does some stuff
        // maybe based on arguments / parameters
        // calculating function output
        result = "output"
    } catch (error) {
        // handle errors
        return [null, error]
    }

    return [result, null]
}

let [funcResult, error] = specialFunc()

if (!error) {
    // do stuff with funcResult
    console.log(funcResult);
}