// create a primitive
let age = 31

// create a non-primitive (JS object or array)
let myPerson = {
    age: 31
}

// pass them into a function that changes them
// void function
function changeValue(value) {
    value -= 1 // - is always a math function
}

function changeObject(object) {
    object.age -= 1
}

// invoke function, and pass by value
changeValue(age)

// invoke function, and pass by value
changeValue(myPerson.age)

// invoke function, and pass by value
changeObject(myPerson)

// inspect what happens
// console.log("age:", age); // 31
// console.log("myPerson.age:", myPerson.age); // 31
// console.log("myPerson:", myPerson); // 30

// primitives
let num = 10
let num2 = num

num *= 2

// console.log("num:", num); // ?
// console.log("num2:", num2); // ?

// non-primitives
let arr = [ 10, 11, 12 ]
// let arr2 = arr.slice()
let arr2 = [...arr] // spread operator

let twoDArr = [ arr, arr2 ]
let copyOfTwoDArr = [...twoDArr]

let person1 = { fname: "Henry", lname: "Ford" }
let person2 = {...person1}

// change the non-primitive
arr.push(13)
twoDArr.push([1, 2, 3])
person1.favColor = "Red"

// arrays
// console.log("arr:", arr); // ?
// console.log("arr2:", arr2); // ?
console.log("twoDArr:", twoDArr); // ?
console.log("copyOfTwoDArr:", copyOfTwoDArr); // ?

// objects
// console.log("person1:", person1); // ?
// console.log("person2:", person2); // ?