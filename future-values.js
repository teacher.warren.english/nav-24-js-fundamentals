const DRINKS_API_URL = "https://my-json-server.typicode.com/SeanNoroff/drinks-api/drinks"
let drinks = []

// ES6 Promises

// use the fetch API
function getDataWithPromise() {
    fetch(DRINKS_API_URL)
        .then((resp) => resp.json()) // promises can be appended with .then() / .catch() / .finally()
        .then((json) => {
            drinks = json

            console.log("line 12", drinks)
            // render this data to the UI
            // or process the data
            // or do whatever you need with the data

            console.log(drinks[0].description, drinks[0].price, drinks[0].id)
        })
        .catch((error) => {
            console.error(error) // will ONLY fire if an error is thrown
        })
    console.log("line 23", drinks)
}


// set timeout
function timeout() {
    setTimeout(() => {
        console.log("Inside the timeout")
        console.log(drinks)
    }, 2000)

    console.log("After the timeout")
}

// ES7 Async + Await

// void function
async function getDataAsync() {
    try {
        const resp = await fetch(DRINKS_API_URL)
        console.log(resp)
    
        const json = await resp.json()
        console.log(json)
    }
    catch (error) {
        console.error(error);
    }
    

    // drinks = json
    // call functions to render API data to components
    // call functions that process the API data (calculations)

    // return json
}

getDataAsync()


// async function useAsyncData() {
//     drinks = await getDataAsync()
//     console.log(drinks);

// }
// useAsyncData()





function logStuff() {

    // Bonus console logging content:
    console.info("This is a console info")
    console.warn("This is a console warning")
    console.error("This is a console error")
}