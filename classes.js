class Animal {
    #secretAttribute;

    constructor(name, emoji, weight) {
        this.name = name
        this.emoji = emoji
        this.weight = weight
        this.#secretAttribute = "private"
    }

    printInfo() {
        let message = `Name: ${this.name}\nEmoji: ${this.emoji}\nWeight: ${this.weight}` // string interpolation
        console.log(message)
    }
}

const garfield = new Animal("Garfield", "🐈", 15)
garfield.printInfo()
console.log(garfield);
// console.log(garfield.#secretAttribute);