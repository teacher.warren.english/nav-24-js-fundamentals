let myAge = 31

// one year passes

myAge = myAge + 1

// console.log(myAge);

myAge++

// console.log(myAge);

myAge += 1

// console.log(myAge);

++myAge

// console.log(myAge);

// time travel

// console.log(myAge--)

// console.log(myAge -= 1)

// console.log(--myAge)

// comparison

let a = "123"
let b = 123
let c = 0
let d = false
let e = true

// == means regular equality
if (a == b) {
    // console.log("Yes");
}

// === means strict equality
if (a === b) {
    // console.log("No");
}

// Coercion

console.log(a + b);
console.log(a - b);
console.log(a + d);
console.log(b + d);
console.log(b - d);
console.log(b + e);
console.log(b - e);

console.log(Boolean(0));
console.log(Boolean(-45));

// checking strings
// is this string empty, null or undefined?

let lname;

console.log(lname);

let fname = null;// = prompt("Enter your name:")

if (!fname) {
    console.log("Name is not valid");
}

undefined // is the value of a variable in memory before it has been assigned any value
null // is a value given to a variable to show it's empty