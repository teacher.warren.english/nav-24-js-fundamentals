// DOM ELEMENTS
// DOM traversal behaviour (should never be inside flow control e.g. if statements / loops)
const headerEl = document.getElementById("header")
const inputTextEl = document.getElementById("input-header-text")
const inputTodoItemEl = document.getElementById("input-add-todo-item")
const btnChangeHeaderEl = document.getElementById("btn-change-header")
const btnAddTodoItemEl = document.getElementById("btn-add-todo-item")
const todoList = document.getElementById("todo-list")

// EVENT LISTENERS
btnChangeHeaderEl.addEventListener("click", handleButtonClick)
btnAddTodoItemEl.addEventListener("click", handleAddTodoItem)

// EVENT HANDLERS
function handleButtonClick() {
    // do some stuff
    console.log("Button clicked!");
    console.log("input value:", inputTextEl.value);
    console.log("header innerText:", headerEl.innerText);

    headerEl.innerText = inputTextEl.value
}

function handleAddTodoItem() {
    // basic validation
    if (!!inputTodoItemEl.value)
        addTodoItem()


    // typeof keyword in JavaScript:
    // console.log(typeof inputTodoItemEl.value);
    // console.log(typeof (inputTodoItemEl.value == "meditate"));
    // console.log(typeof 55);
    // console.log(typeof typeof false);
}


// FUNCTIONS

function addTodoItem() {
    // let todoStuff = ["Eat breakfast", "Meditate", "Walk the cat", "Learn JavaScript"]

    // for (let i = 0; i < todoStuff.length; i++) {
    //     todoList.innerHTML += `<li>${todoStuff[i]}</li>`
    // }

    // adding <li> elements within a parent <ul> element
    todoList.innerHTML += `<li>${inputTodoItemEl.value}</li>`
}

// GLOBAL VARIABLES